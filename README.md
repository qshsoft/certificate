qshsoft/certificate

## 安装

~~~
composer require qshsoft/image @dev
~~~
## 使用

~~~
//图片存证
$image = \Qshsoft\image\Certificate::getInstance();
$image->getOssCertificate('http://xxxxx/xxx.jpg', 'xxxx', time(), "用户昵称限制六个字符", "用户作品标题最大允许44个字节等于22个汉字太长尾部用省略号号代替");
//签约摄影师证书
$image = \Qshsoft\image\Photographer::getInstance();
$image->getOssCard("等级1-4", "姓名", time(), "http://xxxxx/xxx.jpg");
~~~


## 目录结构
```
.
├── README.md
├── composer.json
├── docs
├── src
    └── Certificate.php
    └── Photographer.php
    └── static
        └── fonts
        └── images
```
