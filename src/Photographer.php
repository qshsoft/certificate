<?php
/**
 * 基于阿里云SDK的签约摄影师证书
 * User: kentChen
 * Date: 2019/11/6
 * Time: 11:05
 */

namespace Qshsoft\image;


class Photographer
{
    private static $instance = null;

    protected $staticPath  = '';
    //背景
    protected $background = [];
    //字体
    protected $font = '';
    //图片质量
    protected $quality = 90;
    //画布-宽
    protected $width = 0;
    //画布-高
    protected $height = 0;
    //图片源
    protected $image = '';
    //设置环境
    protected $internal = false;

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {
        $this->staticPath  = dirname(__FILE__).'/static/';
        $this->background = [
            '1' => [$this->staticPath . 'images/photo_1.png', $this->staticPath . 'images/photo_avatar_1.png'],
            '2' => [$this->staticPath . 'images/photo_2.png', $this->staticPath . 'images/photo_avatar_2.png'],
            '3' => [$this->staticPath . 'images/photo_3.png', $this->staticPath . 'images/photo_avatar_3.png'],
            '4' => [$this->staticPath . 'images/photo_4.png', $this->staticPath . 'images/photo_avatar_4.png'],
        ];
        $this->font = $this->staticPath . 'fonts/wryh.ttf';
        
    }

    public function init($params = null)
    {
        if(empty($params)){
            return false;
        }
        if (isset($params['font'])) {
            $this->font = $params['font'];
        }
        if (isset($params['quality'])) {
            $this->quality = $params['quality'];
        }
        if (isset($params['internal'])) {
            $this->internal = $params['internal'];
        }
    }

    /**
     * 生成签约摄影师证书上传阿里云返回CODE和阿里云信息
     * @param $level
     * @param $name
     * @param $time
     * @param $imageUrl
     * @return null
     */
    public function getOssCard($level, $name, $time, $imageUrl)
    {
        if (empty($this->background[$level])) {
            return false;
        }
        $tempImage = $this->getCurlCode($imageUrl);
        if (!$tempImage) {
            return false;
        }
        $background = $this->background[$level][0];
        $backgroundAvatar = $this->background[$level][1];
        list($this->width, $this->height) = getimagesize($background);
        //创建图片
        $this->image = imagecreatefrompng($background);
        //裁剪头像图片小大 为 130 X 130
        $avatarWidth  = 130;
        $avatarHeight = 130;
        $avatar = $this->imageResize($tempImage, $avatarWidth, $avatarHeight);
        imagecopy($this->image, $avatar, ceil(($this->width - $avatarWidth) / 2), 320, 0, 0, $avatarWidth, $avatarHeight);
        //头像覆盖边框
        $avatarBg = imagecreatefrompng($backgroundAvatar);
        imagecopy($this->image, $avatarBg, ceil(($this->width - $avatarWidth) / 2), 320, 0, 0, $avatarWidth, $avatarHeight);
        //用户名
        $font = 26;
        $fontBox = imagettfbbox($font, 0, $this->font, $name);//文字水平居中实质
        $color  = imagecolorallocatealpha($this->image, 0, 0, 0, 0);
        imagettftext($this->image, $font, 0, ceil(($this->width - $fontBox[2]) / 2), 495, $color, $this->font, $name);
        //日期
        $date = date("Y-m-d", $time);
        $font = 18;
        $fontBox = imagettfbbox($font, 0, $this->font, $date);//文字水平居中实质
        $color  = imagecolorallocatealpha($this->image, 0, 0, 0, 0);
        imagettftext($this->image, $font, 0, 495, 995, $color, $this->font, $date);
        //保存本地
        $tempFile = $this->getTempFileName();
        imagejpeg($this->image, $tempFile, $this->quality);
        //上传OSS
        $code = $this->getCode();
        $oss = new \aliyun\Oss($this->internal);
        $url = 'photographer/' . substr($code, 0, 3) . '/' . substr($code, 3, 3) . '/' . $code . '.jpg';
        $oss = $oss->uploadFile($url, $tempFile);
        if (file_exists($tempFile)) {
            @unlink($tempFile);
        }
        if (file_exists($tempImage)) {
            @unlink($tempImage);
        }
        if (!empty($oss) && $oss['info']['http_code'] == 200 ) {
            return str_ireplace("-internal.", ".", $oss['info']['url']);
        }
        return false;
    }

    /**
     * 图像裁剪为任意大小的图像，图像不变形
     * @param string $src_file   需要处理图片的文件名（绝对路径）
     * @param int    $new_width  生成新图片的宽
     * @param int    $new_height 生成新图片的高
     * @return resource
     */
    protected function imageResize($src_file, $new_width, $new_height)
    {
        // 图像信息
        list($w, $h, $type) = getimagesize($src_file);
        //加载图片
        switch ($type) {
            case IMAGETYPE_JPEG:
                $src_img = imagecreatefromjpeg($src_file);
                break;
            case IMAGETYPE_PNG:
                $src_img = imagecreatefrompng($src_file);
                break;
            case IMAGETYPE_GIF:
                $src_img = imagecreatefromgif($src_file);
                break;
            default:
                echo 'Load image error!';
                die;
        }
        $ratio_w = (1.0 * $new_width) / $w;
        $ratio_h = (1.0 * $new_height) / $h;
        $ratio = 1.0;

        // 生成的图像的高宽比原来的都小，或都大 ，原则是 取大比例放大，取大比例缩小（缩小的比例就比较小了）
        if ($ratio_w < 1 && $ratio_h < 1 || $ratio_w > 1 && $ratio_h > 1) {
            if ($ratio_w < $ratio_h) {
                $ratio = $ratio_h;
            } else {
                $ratio = $ratio_w;
            }
            // 定义一个中间的临时图像，该图像的宽高比 正好满足目标要求
            $inter_w = (int)($new_width / $ratio);
            $inter_h = (int)($new_height / $ratio);
            $inter_img = imagecreatetruecolor($inter_w, $inter_h);
            imagecopy($inter_img, $src_img, 0, 0, 0, 0, $inter_w, $inter_h);

            // 生成一个以最大边长度为大小的是目标图像$ratio比例的临时图像
            // 定义一个新的图像
            $new_img = imagecreatetruecolor($new_width, $new_height);
            imagecopyresampled($new_img, $inter_img, 0, 0, 0, 0, $new_width, $new_height, $inter_w, $inter_h);
        } else {
            $ratio = $ratio_h > $ratio_w ? $ratio_h : $ratio_w;

            // 取比例大的那个值
            // 定义一个中间的大图像，该图像的高或宽和目标图像相等，然后对原图放大
            $inter_w = (int)($w * $ratio);
            $inter_h = (int)($h * $ratio);
            $inter_img = imagecreatetruecolor($inter_w, $inter_h);

            // 将原图缩放比例后裁剪
            imagecopyresampled($inter_img, $src_img, 0, 0, 0, 0, $inter_w, $inter_h, $w, $h);

            // 定义一个新的图像
            $new_img = imagecreatetruecolor($new_width, $new_height);
            imagecopy($new_img, $inter_img, 0, 0, 0, 0, $new_width, $new_height);
        }
        return $new_img;
    }

    private function getCurlCode($url){
        $ch = curl_init();
        preg_match('/https:\/\//', $url) ? $ssl = TRUE : $ssl = FALSE;
        if ($ssl) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1');
        $image = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        if (in_array($info['http_code'], ['200', '302'])) {
            $saveName = $this->getTempFileName();
            $file = fopen($saveName,"w");
            fwrite($file, $image);
            fclose($file);
            return $saveName;
        } else {
            \think\Log::write("Photographer error http code :".$info['http_code'], 'debug');
            return false;
        }
    }

    private function getTempFileName()
    {
        return RUNTIME_PATH.'temp/photo'.rand(1000, 9999).'.jpg';
    }

    /**
     * 获取md5 id
     * @return string
     */
    protected function getCode()
    {
        return md5('PT'.date('YmdHis').rand(1000,9999).rand(100, 999));
    }

}