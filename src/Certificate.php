<?php
/**
 * 基于阿里云SDK的图片存证类.
 * User: kentChen
 * Date: 2019/11/6
 * Time: 11:05
 */

namespace Qshsoft\image;

class Certificate
{
    private static $instance = null;

    protected $staticPath  = '';
    //背景
    protected $background = '';
    //水印
    protected $watermark  = '';
    //字体
    protected $font = '';
    //签名字体
    protected $fontQm = '';
    //图片质量
    protected $quality = 90;
    //画布-宽
    protected $width = 0;
    //画布-高
    protected $height = 0;
    //图片源
    protected $image = '';
    //图片规格尺寸
    protected $size = '';
    //设置环境
    protected $internal = false;

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {
        $this->staticPath  = dirname(__FILE__).'/static/';
        $this->background = $this->staticPath . 'images/auth_background.png';
        $this->watermark  = $this->staticPath . 'images/auth_watermark.png';
        $this->font = $this->staticPath . 'fonts/PingFangBold.ttf';
        $this->fontQm = $this->staticPath . 'fonts/simsun.ttf';
        list($this->width, $this->height) = getimagesize($this->background);
    }

    public function init($params = null)
    {
        if(empty($params)){
            return false;
        }
        if (isset($params['imagePath'])) {
            $this->imagePath = $params['imagePath'];
        }
        if (isset($params['background'])) {
            $this->background = $params['background'];
            list($this->width, $this->height) = getimagesize($this->background);
        }
        if (isset($params['watermark'])) {
            $this->watermark = $params['watermark'];
        }
        if (isset($params['font'])) {
            $this->font = $params['font'];
        }
        if (isset($params['quality'])) {
            $this->quality = $params['quality'];
        }
        if (isset($params['internal'])) {
            $this->internal = $params['internal'];
        }
    }

    /**
     * 生成证书上传阿里云返回CODE和阿里云信息
     * @param $imageUrl
     * @param $uid
     * @param $time
     * @param $nickname
     * @param $title
     * @return null
     */
    public function getOssCertificate($imageUrl, $uid, $time, $nickname, $title)
    {
        if (!$this->getCurlCode($imageUrl)) {
            return false;
        }
        $nickname = mb_strwidth($nickname, "utf8") > 11 ? mb_strimwidth($nickname, 0, 11, '...', 'utf8') : $nickname;
        $title    = preg_replace('/\s/i', '', $title);
        $title    = mb_strwidth($title, "utf8") > 32 ? mb_strimwidth($title, 0, 32, '...', 'utf8') : $title;
        $tempFile = $this->getTempFileName();
        $oss = new \aliyun\Oss($this->internal);
        list($code, $fileName)  = $this->getCertificate($imageUrl, $uid, $nickname, $title, $time, $tempFile);
        $url = 'certificate/' . substr($code, 0, 3) . '/' . substr($code, 3, 3) . '/' . $code . '.jpg';
        $oss = $oss->uploadFile($url, $fileName);
        if (!empty($oss) && !empty($oss['info'])) {
            $oss['info']['url'] = str_ireplace("-internal.", ".", $oss['info']['url']);
        }
        $data = [
            'code' => $code,
            'url'  => $url,
            'oss'  => $oss,
        ];
        if (file_exists($tempFile)) {
            @unlink($tempFile);
        }
        return $data;
    }

    /**
     * 生成证书返回CODE和本地文件
     * @param $imageUrl
     * @param $uid
     * @param $nickname
     * @param $title
     * @param $time
     * @param $saveFileName
     * @return array
     */
    public function getCertificate($imageUrl, $uid, $nickname, $title, $time, $saveFileName)
    {
        //创建图片
        $this->image = imagecreatefrompng($this->background);
        //设置认证编号
        $code = $this->setImageCode();
        //设置用户图片
        $this->setUserImage($imageUrl);
        //设置用户ID，时间
        $this->setUserInfo($uid, $time, $nickname, $title);
        //设置日期
        $this->setImageDate($time);
        imagejpeg($this->image, $saveFileName, $this->quality);
        return [$code, $saveFileName];
    }

    /**
     * 获取md5 id
     * @return string
     */
    protected function getCode()
    {
        return md5('QT'.date('YmdHis').rand(1000,9999).rand(100, 999));
    }

    protected function setUserImage($fileName)
    {
        $sizeConf = [
            1 => [
                'width'  => 512, //横图-宽度
                'height' => 384, //横图-高度
                'x' => 118, //目标证书渲染坐标-X
                'y' => 405, //目标证书渲染坐标-Y
            ],
            2 => [
                'width'  => 324, //竖图-宽度
                'height' => 432, //竖图-高度
                'x' => 210, //目标证书渲染坐标-X
                'y' => 400, //目标证书渲染坐标-Y
            ]
        ];
        //获取用户图片的宽高
        list($width, $height) = getimagesize($fileName);
        //简单判断横图，竖图
        $size  = $height > $width ? $sizeConf[2] : $sizeConf[1];
        //裁剪用户图片
        $img = $this->imageResize($fileName, $size['width'], $size['height']);
        //拷贝图像到证书中
        imagecopy($this->image, $img, $size['x'], $size['y'], 0, 0, $size['width'], $size['height']);
        //认证盖章
        list($watermarkW, $watermarkH) = getimagesize($this->watermark);
        $watermarkImage = imagecreatefrompng($this->watermark);
        $watermarkX = $size['x'] + $size['width'] - $watermarkW * 0.7;
        $watermarkY = $size['y'] + $size['height'] - $watermarkH * 0.8;
        imagecopy($this->image, $watermarkImage, $watermarkX, $watermarkY, 0, 0, $watermarkW, $watermarkH);
        $this->size = $size;
    }

    protected function setUserInfo($code, $time, $nickname, $title)
    {
        $infoY = $this->size['y'] + $this->size['height'] + ($this->size['height'] > 384 ? 45 : 65);
        //标题
        $font = 22;
        $fontBox = imagettfbbox($font, 0, $this->font, $title);//文字水平居中实质
        $color  = imagecolorallocatealpha($this->image, 0, 0, 0, 0);
        imagettftext($this->image, $font, 0, ceil(($this->width - $fontBox[2]) / 2), $infoY, $color, $this->font, $title);
        //千图认证ID
        $font = 16;
        $text = "千途用户ID：{$code}的作品";
        $fontBox = imagettfbbox($font, 0, $this->font, $text);//文字水平居中实质
        $color = imagecolorallocatealpha($this->image, 0, 0, 0, 0);
        imagettftext($this->image, $font, 0, ceil(($this->width - $fontBox[2]) / 2), $infoY + 35, $color, $this->font, $text);
        //认证时间
        $font = 12;
        $text = date("已于Y年m月d日H:i完成区块链作品存证", $time);
        $fontBox = imagettfbbox($font, 0, $this->font, $text);//文字水平居中实质
        $color = imagecolorallocatealpha($this->image, 0, 0, 0, 0);
        imagettftext($this->image, $font, 0, ceil(($this->width - $fontBox[2]) / 2), $infoY + 65, $color, $this->font, $text);
        //用户昵称
        $font = 27;
        $fontBox = imagettfbbox($font, 0, $this->fontQm, $nickname);//文字水平居中实质
        $color  = imagecolorallocatealpha($this->image, 0, 0, 0, 0);
        imagettftext($this->image, $font, 0, 481 + max(0, ceil((162 - $fontBox[2]) / 2) ), 1150, $color, $this->fontQm, $nickname);
    }

    /**
     * 设置唯一id
     * @return string
     */
    protected function setImageCode()
    {
        $code = $this->getCode();
        //设置字体颜色和透明度
        $color = imagecolorallocatealpha($this->image, 152, 100, 35, 0);
        imagettftext($this->image, 16, 0, 180, 375, $color, $this->font, $code);
        return $code;
    }

    /**
     * 设置日期
     * @param $time
     */
    protected function setImageDate($time)
    {
        $text = date('Y-m-d', $time);
        //设置字体颜色和透明度
        $color = imagecolorallocatealpha($this->image, 0, 0, 0, 0);
        imagettftext($this->image, 16, 0, 118, 1150, $color, $this->font, $text);
    }

    /**
     * 图像裁剪为任意大小的图像，图像不变形
     * @param string $src_file   需要处理图片的文件名（绝对路径）
     * @param int    $new_width  生成新图片的宽
     * @param int    $new_height 生成新图片的高
     * @return resource
     */
    protected function imageResize($src_file, $new_width, $new_height)
    {
        // 图像信息
        list($w, $h, $type) = getimagesize($src_file);
        //加载图片
        switch ($type) {
            case IMAGETYPE_JPEG:
                $src_img = imagecreatefromjpeg($src_file);
                break;
            case IMAGETYPE_PNG:
                $src_img = imagecreatefrompng($src_file);
                break;
            case IMAGETYPE_GIF:
                $src_img = imagecreatefromgif($src_file);
                break;
            default:
                echo 'Load image error!';
                die;
        }
        $ratio_w = (1.0 * $new_width) / $w;
        $ratio_h = (1.0 * $new_height) / $h;
        $ratio = 1.0;

        // 生成的图像的高宽比原来的都小，或都大 ，原则是 取大比例放大，取大比例缩小（缩小的比例就比较小了）
        if ($ratio_w < 1 && $ratio_h < 1 || $ratio_w > 1 && $ratio_h > 1) {
            if ($ratio_w < $ratio_h) {
                $ratio = $ratio_h;
            } else {
                $ratio = $ratio_w;
            }
            // 定义一个中间的临时图像，该图像的宽高比 正好满足目标要求
            $inter_w = (int)($new_width / $ratio);
            $inter_h = (int)($new_height / $ratio);
            $inter_img = imagecreatetruecolor($inter_w, $inter_h);
            imagecopy($inter_img, $src_img, 0, 0, 0, 0, $inter_w, $inter_h);

            // 生成一个以最大边长度为大小的是目标图像$ratio比例的临时图像
            // 定义一个新的图像
            $new_img = imagecreatetruecolor($new_width, $new_height);
            imagecopyresampled($new_img, $inter_img, 0, 0, 0, 0, $new_width, $new_height, $inter_w, $inter_h);
        } else {
            $ratio = $ratio_h > $ratio_w ? $ratio_h : $ratio_w;

            // 取比例大的那个值
            // 定义一个中间的大图像，该图像的高或宽和目标图像相等，然后对原图放大
            $inter_w = (int)($w * $ratio);
            $inter_h = (int)($h * $ratio);
            $inter_img = imagecreatetruecolor($inter_w, $inter_h);

            // 将原图缩放比例后裁剪
            imagecopyresampled($inter_img, $src_img, 0, 0, 0, 0, $inter_w, $inter_h, $w, $h);

            // 定义一个新的图像
            $new_img = imagecreatetruecolor($new_width, $new_height);
            imagecopy($new_img, $inter_img, 0, 0, 0, 0, $new_width, $new_height);
        }
        return $new_img;
    }

    private function getCurlCode($url){
        $ch = curl_init();
        preg_match('/https:\/\//', $url) ? $ssl = TRUE : $ssl = FALSE;
        if ($ssl) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        if (in_array($info['http_code'], ['200', '302'])) {
            return true;
        } else {
            \think\Log::write("Certificate error http code :".$info['http_code'], 'debug');
            return false;
        }
    }

    private function getTempFileName()
    {
        return RUNTIME_PATH.'temp/temp'.rand(1000, 9999).'.jpg';
    }

}